package replicatorg.plugin.toolpath;

/**
 * Copyright (c) 2013 BEEVC - Electronic Systems
 */

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import replicatorg.app.ProperDefault;

import replicatorg.app.Base;
import replicatorg.plugin.toolpath.skeinforge.SkeinforgeGenerator;

public class ToolpathGeneratorFactory {
	public static class ToolpathGeneratorDescriptor {
		public String name;
		public String description;
		public Class<?> tpClass;
		
		public ToolpathGeneratorDescriptor(String name, String description, 
				Class<?> tpClass) {
			this.name = name;
			this.description = description;
			this.tpClass = tpClass;
		}
	
		public ToolpathGenerator instantiate() {
			try {
				return (ToolpathGenerator)tpClass.newInstance();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			return null;
		}
	}
	
	static private Vector<ToolpathGeneratorDescriptor> generatorList = null;
	
	public static Vector<ToolpathGeneratorDescriptor> getGeneratorList() {
		if (generatorList == null) {
			generatorList = buildGeneratorList();
		}
		return generatorList;
	}
	static private Vector<ToolpathGeneratorDescriptor> buildGeneratorList() {
		Vector<ToolpathGeneratorDescriptor> list = new Vector<ToolpathGeneratorDescriptor>();
		
//		class Skeinforge44 extends SkeinforgeGenerator {
//			public File getDefaultSkeinforgeDir() {
//		    	return Base.getApplicationFile("skein_engines/skeinforge-44/skeinforge_application");
//			}
//			public File getUserProfilesDir() {
//		    	return Base.getUserFile("sf_44_profiles");
//			}
//			public List<SkeinforgePreference> getPreferences() {
//				List <SkeinforgePreference> prefs = new LinkedList<SkeinforgePreference>();
//				
//				//R2C2: RepRaps don't have this -- Sure they do!
////				PrintOMatic5D printOMatic5D = new PrintOMatic5D();
////				prefs.add(printOMatic5D);
////				
//				return prefs;
//			}
//			@Override
//			public File getGeneratedToolpath() {
//				// TODO Auto-generated method stub
//				return null;
//			}
//		};
                
		class Skeinforge50 extends SkeinforgeGenerator {
			public File getDefaultSkeinforgeDir() {
		    	return Base.getApplicationFile("skein_engines/skeinforge-50/skeinforge_application");
			}
			public File getUserProfilesDir() {
		    	return Base.getUserFile("sf_50_profiles");
			}
			public List<SkeinforgeGenerator.SkeinforgePreference> getPreferences() {
				List <SkeinforgeGenerator.SkeinforgePreference> prefs = new LinkedList<SkeinforgeGenerator.SkeinforgePreference>();
//				
				return prefs;
			}
			@Override
			public File getGeneratedToolpath() {
				// TODO Auto-generated method stub
				return null;
			}
		};                
		
		//R2C2
		if((new Skeinforge50()).getDefaultSkeinforgeDir().exists())
			list.add(new ToolpathGeneratorDescriptor("Skeinforge (50)", 
				"This is a recent version of skeinforge.", Skeinforge50.class));
//		if((new Skeinforge44()).getDefaultSkeinforgeDir().exists())
//			list.add(new ToolpathGeneratorDescriptor("Skeinforge (44)", 
//				"This is a recent version of skeinforge.", Skeinforge44.class));
		
		return list;
	}

	static public String getSelectedName() {
		String name = ProperDefault.get("replicatorg.generator.name");
		return name;
	}

	static public void setSelectedName(String name) {
               ProperDefault.put("replicatorg.generator.name", name);
//		Base.preferences.put("replicatorg.generator.name", name);
	}

	static public ToolpathGenerator createSelectedGenerator() {
		String name = getSelectedName();
		Vector<ToolpathGeneratorDescriptor> list = getGeneratorList();
		ToolpathGenerator tg = null;
		for (ToolpathGeneratorDescriptor tgd : list) {
			if (name.equals(tgd.name)) { tg = tgd.instantiate(); break; }
		}
		return tg;
	}
}

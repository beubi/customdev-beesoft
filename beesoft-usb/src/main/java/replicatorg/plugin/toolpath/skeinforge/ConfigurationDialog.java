package replicatorg.plugin.toolpath.skeinforge;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;
import replicatorg.app.ProperDefault;
import replicatorg.app.Base;
import replicatorg.app.ui.MainWindow;
import replicatorg.plugin.toolpath.skeinforge.SkeinforgeGenerator.Profile;

public class ConfigurationDialog extends JDialog {
	final boolean postProcessToolheadIndex = true;
	final String profilePref = "replicatorg.skeinforge.profilePref";
	
	JButton generateButton = new JButton("Print Model");
	JButton cancelButton = new JButton("Cancel");
	private List<Profile> profiles = null;
	
	JPanel profilePanel = new JPanel();
	
	private void loadList(JComboBox comboBox) {
		comboBox.removeAllItems();
		profiles = getProfiles();
		DefaultComboBoxModel model = new DefaultComboBoxModel();
		int i=0;
		int foundLastProfile = -1;
		for (Profile p : profiles) {
			model.addElement(p.toString());
			if(p.toString().equals(ProperDefault.get("lastGeneratorProfileSelected")))
			{
				Base.logger.fine("Selecting last used element: " + p);
				foundLastProfile = i;
			}
			i++;
		}
		comboBox.setModel(model);
		if(foundLastProfile != -1) {
			comboBox.setSelectedIndex(foundLastProfile);
		}
	}

        private List<Profile> getProfiles()
        {
		final List<Profile> profiles = new LinkedList<Profile>(); 
		// Get default installed profiles
		File dir = new File(getSkeinforgeDir(), "prefs");
		getProfilesIn(dir, profiles);
		dir = Base.getUserFile("sf_44_profiles");
		getProfilesIn(dir, profiles);
		Collections.sort(profiles);
		return profiles;
	}
        
        private void getProfilesIn(File dir, List<Profile> profiles) 
        {
		if (dir.exists() && dir.isDirectory()) {
			for (String subpath : dir.list()) {
				File subDir = new File(dir, subpath);
				if (subDir.isDirectory()) {
					profiles.add(new Profile(subDir.getAbsolutePath()));
				}
			}
		}
	}
        
        private File getSkeinforgeDir() 
        {
		String skeinforgePath = Base.getApplicationDirectory()+"/skein_engines/skeinforge-44/skeinforge_application/";
		if (skeinforgePath == null || (skeinforgePath.length() == 0)) {
			return null;
		}
		return new File(skeinforgePath);
	}
        
	final JComboBox prefPulldown = new JComboBox();

	private Profile getListedProfile(int idx) {
		return profiles.get(idx);
	}

        
	public ConfigurationDialog(MainWindow main) {
		super(main);
		setTitle("GCode Generator");
		setLayout(new MigLayout("aligny, top, ins 5, fill"));
		
		// have to set this. Something wrong with the initial use of the
		// ListSelectionListener
		generateButton.setEnabled(false);
		
		add(new JLabel("Base Profile:"), "split 2");
		
		// This is intended to fix a bug where the "Generate Gcode" button doesn't get enabled 
		prefPulldown.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				generateButton.setEnabled(true);
				generateButton.requestFocusInWindow();
				generateButton.setFocusPainted(true);
			}
			
		});
		loadList(prefPulldown);
		prefPulldown.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				String value = (String)prefPulldown.getSelectedItem().toString();
			}
		});
		add(prefPulldown, "wrap, growx");

		add(generateButton, "tag ok, split 2");
		add(cancelButton, "tag cancel");
		generateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				int idx = prefPulldown.getSelectedIndex();
				Profile p = getListedProfile(idx);
                                ProperDefault.put("lastGeneratorProfileSelected",p.toString());
                                Base.writeLog("Profile Selected: ".concat(p.toString()));
				setVisible(false);
				SkeinforgeGenerator.setSelectedProfile(p.toString());
			}
		});
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				Base.getMachineLoader().getMachineInterface().runCommand(new replicatorg.drivers.commands.SetTemperature(0)); 
				Base.getMachineLoader().getMachineInterface().runCommand(new replicatorg.drivers.commands.DisableFan());
			}
		});
              
	}
};

package replicatorg.app.ui.panels;

import java.awt.Color;
import java.awt.Dimension;
import static java.awt.Frame.ICONIFIED;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import replicatorg.app.Languager;
import replicatorg.app.ui.GraphicDesignComponents;
import replicatorg.app.Base;
import replicatorg.app.DoNotSleep;
import replicatorg.app.Printer;
import replicatorg.machine.MachineInterface;
import replicatorg.plugin.toolpath.skeinforge.SkeinforgeGenerator;
import replicatorg.util.Point5d;


/**
* Copyright (c) 2013 BEEVC - Electronic Systems
* This file is part of BEESOFT software: you can redistribute it and/or modify 
* it under the terms of the GNU General Public License as published by the 
* Free Software Foundation, either version 3 of the License, or (at your option)
* any later version. BEESOFT is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
* for more details. You should have received a copy of the GNU General
* Public License along with BEESOFT. If not, see <http://www.gnu.org/licenses/>.
*/
public class PrintSplashSimple extends javax.swing.JFrame implements WindowListener {

    private Printer prt;
    private boolean printSoonPressed;
    private ArrayList<String> preferences;
    private boolean gcodeEnded;
    private int posX=0,posY=0;
    private long updateSleep;
    private MachineInterface machine;
    private double temperatureGoal;
    private UpdateThread1 ut1;
    private UpdateThread2 ut;
    private double progression;
    
    public PrintSplashSimple(ArrayList<String> prefs) {
        initComponents();
        setFont();
        setTextLanguage();
        centerOnScreen();
        printSoonPressed = false;
        preferences = prefs;
        prt = new Printer(preferences);
        updateSleep = 700;
        progression = 0.0;
        disableSleep();
        enableDrag();
        ut1 = new UpdateThread1(this);
        ut = new UpdateThread2(this,ut1);
        evaluateInitialConditions();
        addWindowListener(this);
        //turn off blower before heating
        machine.runCommand(new replicatorg.drivers.commands.DispatchCommand("M107"));
        machine.runCommand(new replicatorg.drivers.commands.SetTemperature(temperatureGoal));
        setProgressBarColor();
        ut.start();
        setIconImage(new ImageIcon(Base.getImage("images/icon.png", this)).getImage());
    }

    private void setFont()
    {
        jLabel1.setFont(GraphicDesignComponents.getSSProBold("14"));
        jLabel2.setFont(GraphicDesignComponents.getSSProRegular("12"));
        jLabel4.setFont(GraphicDesignComponents.getSSProRegular("12"));
        jLabel11.setFont(GraphicDesignComponents.getSSProRegular("12"));
    }
    
    private void setTextLanguage()
    {
        jLabel1.setText(Languager.getTagValue("Print", "Print_Processing"));
        jLabel2.setText(Languager.getTagValue("Print", "Print_Info"));
        jLabel4.setText(Languager.getTagValue("Print", "Print_InfoLowStatus")); 
        jLabel11.setText(Languager.getTagValue("OptionPaneButtons", "Line3")); 
    }
    
    private void centerOnScreen()
    {
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
 
        // Determine the new location of the window
        int w = this.getSize().width;
        int h = this.getSize().height;
        int x = (dim.width-w)/2;
        int y = (dim.height-h)/2;

        // Move the window
        this.setLocationRelativeTo(null);
        this.setLocationRelativeTo(Base.getMainWindow());          
    }
    
    private void setProgressBarColor()
    {
        jProgressBar1.setForeground(new Color(255, 203, 5));
    }
    
    public void setSleepValue(long val)
    {
        updateSleep = val;
    }
    
    public void setProgression(double prog)
    {
        progression = prog;
    }
    
    private void evaluateInitialConditions()
    {
        temperatureGoal = 220;
        machine = Base.getMachineLoader().getMachineInterface();
        jLabel3.setVisible(false);
        jLabel4.setVisible(false);
        printSoonPressed = true;
        jProgressBar1.setIndeterminate(true);
  
        if(Base.getMainWindow().getBed().isGcodeOK())
        {
            gcodeEnded = true;
            ut1.setGCodeDone(true);
        }
        else
        {
            gcodeEnded = false;
            ut1.start();
        }
    }
    
    public boolean evaluateProgress()
    {
        machine.runCommand(new replicatorg.drivers.commands.ReadTemperature());
        
        try {
            Thread.sleep(updateSleep);
        } catch (InterruptedException ex) {
            Logger.getLogger(PrintSplashSimple.class.getName()).log(Level.SEVERE, null, ex);
        }        
        
        double temperature = machine.getDriverQueryInterface().getTemperature();
        
        
       if((gcodeEnded && temperature < temperatureGoal))
        {
            return false;
        } else if(temperature > temperatureGoal && gcodeEnded)
        {
            firePrint();
            return true;
        } else if(!gcodeEnded || temperature < temperatureGoal)
        {
            return false;
        }

        return false;
    }
    
    private void firePrint()
    {
       if(printSoonPressed )
       {   
           dispose();
           PrintSplashSimpleWaiting p = new PrintSplashSimpleWaiting(preferences,prt);
           p.setVisible(true);
           disableSleep();
           // Save profile for better Oracle GCode 
           SkeinforgeGenerator.setSelectedProfile(preferences.get(0));
       }
       else
       {
//           gcodeEnded = true;
           jLabel11.setText(Languager.getTagValue("ToolPath", "Line19")); 
           jLabel1.setVisible(false);
           jLabel2 .setFont(GraphicDesignComponents.getSSProBold("14"));
           jLabel2.setText(Languager.getTagValue("Print", "Print_Splash_Title"));
//           jLabel11.setIcon(new ImageIcon(RESOURCES_PATH+"b_simple_14.png"));
       }
    }
      
    public int getBarActualValue()
    {
        return jProgressBar1.getValue();
    }
    
    public double generateGCode()
    {   
       return prt.generateGCode(preferences);    
    }
    
    private void terminateSkeinforge()
    {
        prt.endGCodeGeneration();
    }

    public void enableSleep()
    {
        try {

            DoNotSleep ds = new DoNotSleep();
            ds.EnabledSleep();

            Base.writeLog("Sleep started!");

        } catch (Exception ex){
            Base.writeLog("Error starting Sleep!");
        }
    }
    
    public void setGCodeFinish(boolean state)
    {
        this.gcodeEnded = state;
    }
    
    public void disableSleep()
    {
        try {

            DoNotSleep ds = new DoNotSleep();
            ds.DisableSleep();

            Base.writeLog("Sleep stoped!");

        } catch (Exception ex){
            Base.writeLog("Error stoping Sleep!");
        }
    }
    
    public void cancelProcess()
    {
            dispose();
            enableSleep();
            Base.bringAllWindowsToFront();       
            Base.cleanDirectoryTempFiles(Base.getAppDataDirectory()+"/3DModels");
            Base.getMainWindow().handleStop();
            Base.getMainWindow().setEnabled(true);
            Base.getMainWindow().getButtons().updatePressedStateButton("print");
            Base.getMainWindow().getMachine().runCommand(new replicatorg.drivers.commands.SetTemperature(0));
            ut.stop();
            ut1.stop();  
            Base.getMainWindow().getBed().setGcodeOK(false);
            terminateSkeinforge();
            Point5d b = machine.getTablePoints("safe");
            double acLow = machine.getAcceleration("acLow");
            double acHigh = machine.getAcceleration("acHigh");
            double spHigh = machine.getFeedrate("spHigh");

            machine.runCommand(new replicatorg.drivers.commands.DispatchCommand("M206 x"+acLow));
            machine.runCommand(new replicatorg.drivers.commands.SetFeedrate(spHigh));
            machine.runCommand(new replicatorg.drivers.commands.QueuePoint(b));
            machine.runCommand(new replicatorg.drivers.commands.DispatchCommand("M206 x"+acHigh));
            machine.runCommand(new replicatorg.drivers.commands.DispatchCommand("G28"));
    }
   
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jProgressBar1 = new javax.swing.JProgressBar();
        jPanel4 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);

        jPanel2.setBackground(new java.awt.Color(248, 248, 248));

        jLabel1.setText("A PROCESSAR... POR FAVOR AGUARDE");

        jLabel2.setText("Este processo podera levar varios minutos");

        jProgressBar1.setBackground(new java.awt.Color(186, 186, 186));
        jProgressBar1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jProgressBar1StateChanged(evt);
            }
        });

        jPanel4.setBackground(new java.awt.Color(255, 203, 5));
        jPanel4.setMinimumSize(new java.awt.Dimension(62, 26));
        jPanel4.setRequestFocusEnabled(false);

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/replicatorg/app/ui/panels/b_simple_11.png"))); // NOI18N
        jLabel13.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel13MousePressed(evt);
            }
        });

        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/replicatorg/app/ui/panels/b_simple_10.png"))); // NOI18N

        jLabel15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/replicatorg/app/ui/panels/b_simple_9.png"))); // NOI18N
        jLabel15.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel15MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 7, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, 13, Short.MAX_VALUE)
                            .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(424, 424, 424)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jProgressBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(150, 150, 150)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addContainerGap())))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 19, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 48, Short.MAX_VALUE)
                .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 8, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22))
        );

        jPanel3.setBackground(new java.awt.Color(255, 203, 5));
        jPanel3.setMinimumSize(new java.awt.Dimension(20, 46));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/replicatorg/app/ui/panels/c_unchecked.png"))); // NOI18N
        jLabel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel3MouseClicked(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Ubuntu", 0, 12)); // NOI18N
        jLabel4.setText("INCIAR A IMPRESSAO LOGO QUE POSSIVEL");

        jLabel11.setForeground(new java.awt.Color(0, 0, 0));
        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/replicatorg/app/ui/panels/b_simple_13.png"))); // NOI18N
        jLabel11.setText("CANCELAR");
        jLabel11.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel11MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel11MouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel11MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jLabel3)
                .addGap(6, 6, 6)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel11)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel3))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(jLabel11))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jProgressBar1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jProgressBar1StateChanged
       

    }//GEN-LAST:event_jProgressBar1StateChanged

    private void jLabel3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseClicked
        if(!printSoonPressed)
        {
            jLabel3.setIcon(new ImageIcon(GraphicDesignComponents.getImage("panels","c_checked.png")));
            printSoonPressed = true;
        }
        else
        {
            jLabel3.setIcon(new ImageIcon(GraphicDesignComponents.getImage("panels","c_unchecked.png")));
            printSoonPressed = false;
        }
    }//GEN-LAST:event_jLabel3MouseClicked

    private void jLabel11MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel11MouseEntered
        jLabel11.setIcon(new ImageIcon(GraphicDesignComponents.getImage("panels", "b_hover_13.png")));
    }//GEN-LAST:event_jLabel11MouseEntered

    private void jLabel11MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel11MouseExited
        jLabel11.setIcon(new ImageIcon(GraphicDesignComponents.getImage("panels", "b_simple_13.png")));
    }//GEN-LAST:event_jLabel11MouseExited

    private void jLabel11MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel11MousePressed

        if(jProgressBar1.getValue()== 100 )
        {
           dispose();
           PrintSplashSimpleWaiting p = new PrintSplashSimpleWaiting(preferences,prt);
           p.setVisible(true);
           disableSleep();
           ut.stop();
           ut1.stop();  
           terminateSkeinforge();
        }
        else
        {
            cancelProcess();          
        }
    }//GEN-LAST:event_jLabel11MousePressed

    private void jLabel13MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel13MousePressed
        setState(ICONIFIED);
        Base.getMainWindow().setState(ICONIFIED);
        Base.getMainWindow().deactivateCameraControls();
    }//GEN-LAST:event_jLabel13MousePressed

    private void jLabel15MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel15MousePressed
        dispose();
        enableSleep();
        Base.bringAllWindowsToFront();        
        Base.getMainWindow().handleStop();
        Base.getMainWindow().setEnabled(true);
        Base.getMainWindow().getButtons().updatePressedStateButton("print");
        Base.getMainWindow().getMachine().runCommand(new replicatorg.drivers.commands.SetTemperature(0));
        Point5d b = machine.getTablePoints("safe");
        double acLow = machine.getAcceleration("acLow");
        double acHigh = machine.getAcceleration("acHigh");
        double spHigh = machine.getFeedrate("spHigh");

        machine.runCommand(new replicatorg.drivers.commands.DispatchCommand("M206 x"+acLow));
        machine.runCommand(new replicatorg.drivers.commands.SetFeedrate(spHigh));
        machine.runCommand(new replicatorg.drivers.commands.QueuePoint(b));
        machine.runCommand(new replicatorg.drivers.commands.DispatchCommand("M206 x"+acHigh));
        machine.runCommand(new replicatorg.drivers.commands.DispatchCommand("G28"));
    }//GEN-LAST:event_jLabel15MousePressed

    private void enableDrag()
    {
        this.addMouseListener(new MouseAdapter()
        {
           public void mousePressed(MouseEvent e)
           {
              posX=e.getX();
              posY=e.getY();
           }
        });
        
        
        this.addMouseMotionListener(new MouseAdapter()
        {
             public void mouseDragged(MouseEvent evt)
             {
                //sets frame position when mouse dragged			
                setLocation (evt.getXOnScreen()-posX,evt.getYOnScreen()-posY);
             }
        });
    }   
    
    @Override
    public void windowOpened(WindowEvent e) {
        return;
    }

    @Override
    public void windowClosing(WindowEvent e) {
        return;
    }

    @Override
    public void windowClosed(WindowEvent e) {
        return;
    }

    @Override
    public void windowIconified(WindowEvent e) {
        Base.getMainWindow().setState(JFrame.ICONIFIED);
        return;
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        jProgressBar1.setValue((int)progression);
        Base.getMainWindow().setExtendedState(JFrame.MAXIMIZED_BOTH);
    }

    @Override
    public void windowActivated(WindowEvent e) {
        return;
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        return;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JProgressBar jProgressBar1;
    // End of variables declaration//GEN-END:variables
}

class UpdateThread2 extends Thread {

    PrintSplashSimple window;
    UpdateThread1 ut1;
            
    public UpdateThread2(PrintSplashSimple w, UpdateThread1 utN ) {
        super("Update Thread");
        this.window = w;  
        this.ut1 = utN;
    }

    @Override
    public void run() {
        boolean gcodeGenerated = false;
    
        while (!gcodeGenerated) {
            try {

                if(window.evaluateProgress())
                    gcodeGenerated = true;
                 
                 if(ut1.getGCodeDone() )
                 {
                     window.setGCodeFinish(true);
                     window.setSleepValue(100);
                 } 
            } catch(Exception e)
            {
                break;
            }
        }

    }
}

class UpdateThread1 extends Thread {

    PrintSplashSimple window;
    boolean finish = false;
    double estimatedTime = 0;
    boolean gCodeDone = false;
    
    public UpdateThread1(PrintSplashSimple w) {
        super("Update Thread");
        window = w;  
    }
    
    public boolean isReady()
    {
        return finish;
    }
    
    public double getGenerationTime()
    {
        return estimatedTime;
    }
    
    public boolean getGCodeDone()
    {
        return gCodeDone;
    }
    
    public void setGCodeDone(boolean state)
    {
        gCodeDone = state;
    }    

    @Override
    public void run() {
        Base.writeLog("GCode will be generated ...");
        estimatedTime = window.generateGCode();
        
        if(estimatedTime == -1)
        {
            // Error occurred - permissions maybe
            // Cancel print and setting message
            // 5000 ms delay to ensure user reads it
            
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                Logger.getLogger(UpdateThread1.class.getName()).log(Level.SEVERE, null, ex);
            }
                        
            Base.getMainWindow().showFeedBackMessage("gcodeGeneration");
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                Logger.getLogger(UpdateThread1.class.getName()).log(Level.SEVERE, null, ex);
            }
            window.cancelProcess();
        }
        
        Base.writeLog("New GCode generated ...");
        gCodeDone = true;
    }
}


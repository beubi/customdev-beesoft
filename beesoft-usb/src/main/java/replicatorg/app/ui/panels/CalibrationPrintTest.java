package replicatorg.app.ui.panels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import static java.awt.Frame.ICONIFIED;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import pt.bitbox.replicatorg.drivers.usb.UsbPassthroughDriver;
import pt.bitbox.replicatorg.drivers.usb.UsbPassthroughDriver.COM;
import replicatorg.app.Base;
import replicatorg.app.CalibrationGCoder;
import replicatorg.app.Languager;
import replicatorg.app.Printer;
import replicatorg.app.ProperDefault;
import replicatorg.app.ui.GraphicDesignComponents;
import replicatorg.machine.MachineInterface;
import replicatorg.util.Point5d;



/**
 * Copyright (c) 2013 BEEVC - Electronic Systems This file is part of BEESOFT
 * software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version. BEESOFT is
 * distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details. You
 * should have received a copy of the GNU General Public License along with
 * BEESOFT. If not, see <http://www.gnu.org/licenses/>.
 */
public class CalibrationPrintTest extends javax.swing.JFrame {

    private MachineInterface machine;
    private boolean achievement;
    private boolean quickGuide;
    private int posX = 0, posY = 0;
    private double temperatureGoal;
    private PrintThread updateThread;
    private ArrayList<String> preferences;
    private BufferedReader reader = null;
    public boolean okEnabled;

    public CalibrationPrintTest() {
        initComponents();
        setFont();
        setTextLanguage();
//        enableDrag();
        machine = Base.getMachineLoader().getMachineInterface();
        machine.getDriverQueryInterface().resetToolTemperature();
        evaluateInitialConditions();        
        centerOnScreen();
        setProgressBarColor();
        moveToPosition();
        updateThread = new PrintThread(this,machine);
        updateThread.start();
        Base.maintenanceWizardOpen = true;
        Base.systemThreads.add(updateThread);
        machine.runCommand(new replicatorg.drivers.commands.ReadTemperature());
        setIconImage(new ImageIcon(Base.getImage("images/icon.png", this)).getImage());
    }

    private void setFont() {
        jLabel1.setFont(GraphicDesignComponents.getSSProRegular("14"));
        jLabel3.setFont(GraphicDesignComponents.getSSProBold("12"));
        jLabel4.setFont(GraphicDesignComponents.getSSProRegular("12"));
        jLabel7.setFont(GraphicDesignComponents.getSSProRegular("14"));
        jLabel19.setFont(GraphicDesignComponents.getSSProRegular("12"));

    }

    private void setTextLanguage() {
        jLabel1.setText(Languager.getTagValue("FilamentWizard", "Title1"));
        jLabel3.setText(Languager.getTagValue("Print", "Print_Splash_Gcode_Title"));
        jLabel4.setText(splitString(Languager.getTagValue("Print", "Print_Calibration_Info")));
        jLabel7.setText(Languager.getTagValue("FeedbackLabel", "MovingMessage"));
        jLabel19.setText(Languager.getTagValue("OptionPaneButtons", "Line3"));

    }

    private void centerOnScreen() {
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

        // Determine the new location of the window
        int w = this.getSize().width;
        int h = this.getSize().height;
        int x = (dim.width - w) / 2;
        int y = (dim.height - h) / 2;

        // Move the window
//        this.setLocation(x, y);
        this.setLocationRelativeTo(null);
        this.setLocationRelativeTo(Base.getMainWindow());
    }

    private String splitString(String s) {
        int width = 436;
        return buildString(s.split("\\."), width);
    }

    private String buildString(String[] parts, int width) {
        String text = "";
        String ihtml = "<html>";
        String ehtml = "</html>";
        String br = "<br>";

        for (int i = 0; i < parts.length; i++) {
            if (i + 1 < parts.length) {
                if (getStringPixelsWidth(parts[i]) + getStringPixelsWidth(parts[i + 1]) < width) {
                    text = text.concat(parts[i]).concat(".").concat(parts[i + 1]).concat(".").concat(br);
                    i++;
                } else {
                    text = text.concat(parts[i]).concat(".").concat(br);
                }
            } else {
                text = text.concat(parts[i]).concat(".");
            }
        }

        return ihtml.concat(text).concat(ehtml);
    }

    private int getStringPixelsWidth(String s) {
        Graphics g = getGraphics();
        FontMetrics fm = g.getFontMetrics(GraphicDesignComponents.getSSProRegular("10"));
        return fm.stringWidth(s);
    }

    private void setProgressBarColor() {
        jProgressBar1.setForeground(new Color(255, 203, 5));
    }

    public boolean getAchievement() {
        return achievement;
    }

    public void sinalizeHeatSuccess() {
        disableMessageDisplay();
        machine.runCommand(new replicatorg.drivers.commands.DispatchCommand("M300"));
    }

    public void updateHeatBar() {

        if (achievement) {
            return;
            
        }
        machine.runCommand(new replicatorg.drivers.commands.ReadTemperature());
        double temperature = machine.getDriverQueryInterface().getTemperature();
        double hbp; //halfBarProgress
        hbp = (temperature/temperatureGoal)*50;
        double current = jProgressBar1.getValue();
        
        if( current < hbp ){
            jProgressBar1.setValue((int)current+1);
        }

//        if (temperature > (int) (jProgressBar1.getValue() * 2) ) {
//            
//            if((int) (jProgressBar1.getValue() /4) < (int) (temperature / 4) )
//                jProgressBar1.setValue((int) (temperature / 4));            
//        }

        if (temperature < temperatureGoal) {
            achievement = false;
        } else {
            achievement = true;
            jProgressBar1.setValue(50);
        }
    }
    
    public void updatePrintBar() {

        int currentValue = jProgressBar1.getValue();
        jProgressBar1.setValue(currentValue+10);
        
    }
    
    public void updatePrintBar(double p) {

        //int currentValue = jProgressBar1.getValue();
        jProgressBar1.setValue(50 + (int)(p/2));
        
    }
    

    private void enableMessageDisplay() {
        jPanel3.setBackground(new Color(255, 205, 3));
        jLabel7.setForeground(new Color(0, 0, 0));
    }

    public void disableMessageDisplay() {
        jPanel3.setBackground(new Color(248, 248, 248));
        jLabel7.setForeground(new Color(248, 248, 248));
    }

    public void showMessage() {
        enableMessageDisplay();
        jLabel7.setText(Languager.getTagValue("FeedbackLabel", "HeatingMessage"));
    }
    
    public void showPrintMessage() {
        enableMessageDisplay();
        jLabel7.setText(Languager.getTagValue("FeedbackLabel", "MovingMessage"));
    }

    private void moveToPosition() {
        Base.writeLog("Heating...");
        Point5d heat = machine.getTablePoints("heat");

        double acHigh = machine.getAcceleration("acHigh");
        double acMedium = machine.getAcceleration("acMedium");
        double spHigh = machine.getFeedrate("spHigh");

        machine.runCommand(new replicatorg.drivers.commands.SetBusy(true));
        
        //turn off blower before heating
        machine.runCommand(new replicatorg.drivers.commands.DispatchCommand("M107"));
            
        machine.runCommand(new replicatorg.drivers.commands.SetTemperature(temperatureGoal));
        machine.runCommand(new replicatorg.drivers.commands.DispatchCommand("M206 x" + acMedium));
        machine.runCommand(new replicatorg.drivers.commands.SetFeedrate(spHigh));
        machine.runCommand(new replicatorg.drivers.commands.QueuePoint(heat));
        machine.runCommand(new replicatorg.drivers.commands.DispatchCommand("M206 x" + acHigh));
        machine.runCommand(new replicatorg.drivers.commands.SetBusy(false));

    }

    private void finalizeHeat() {
        Base.writeLog("Cooling down...");
        machine.runCommand(new replicatorg.drivers.commands.SetTemperature(0));
    }

    private void enableDrag() {
        this.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                posX = e.getX();
                posY = e.getY();
            }
        });


        this.addMouseMotionListener(new MouseAdapter() {
            public void mouseDragged(MouseEvent evt) {
                //sets frame position when mouse dragged			
                setLocation(evt.getXOnScreen() - posX, evt.getYOnScreen() - posY);

            }
        });
    }

    private void evaluateInitialConditions() {
        achievement = false;
        temperatureGoal = 220;
        okEnabled = false;
        Base.getMainWindow().setEnabled(false);
        disableMessageDisplay();

        preferences = new ArrayList<String>();
        preferences.add("LOW");
        preferences.add(parseCoilCode());
        preferences.add("LOW");
        preferences.add(String.valueOf(false));
        preferences.add(String.valueOf(false));
        try {
            reader = new BufferedReader(new FileReader(getPrintFile()));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CalibrationPrintTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        jProgressBar1.setValue(0);

    }

    private String parseCoilCode() {
        String colorRatio = "1.1164";
        String code = ProperDefault.get("coilCode");

        if (code.contains("301")) {
            colorRatio = "1.1164";
        }
        if (code.contains("302")) {
            colorRatio = "1.0797";
        }
        if (code.contains("303")) {
            colorRatio = "1.2100";
        }
        if (code.contains("304")) {
            colorRatio = "1.0996";
        }
        if (code.contains("305")) {
            colorRatio = "1.1940";
        }
        if (code.contains("306")) {
            colorRatio = "1.2438";
        }
        if (code.contains("321")) {
            colorRatio = "1.2600";
        }
        if (code.contains("322")) {
            colorRatio = "1.1592";
        }

        return colorRatio;
    }
    
    private File getPrintFile()
    {
        File gcode = null;
        PrintWriter pw = null; 
        
        try {
            gcode = new File(Base.getAppDataDirectory()+"/3DModels/temp.gcode");
            pw = new PrintWriter(new FileOutputStream(gcode));
    
            String[] code = CalibrationGCoder.getColorGCode();
            
            for(int i = 0; i < code.length; i++)
                pw.println(code[i].trim());
            
            pw.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CalibrationPrintTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return gcode;
    }

    public int printTest() {

//                        if (jProgressBar1.getValue() == 50) {
        machine.runCommand(new replicatorg.drivers.commands.DispatchCommand("M107"));
        jLabel1.setText(Languager.getTagValue("Print", "Print_Title"));

        String sCurrentLine = "";
        int n_lines = 0;
        try {
            machine.setStopwatch(0);
            machine.getDriverQueryInterface().setBusy(true);
            while ((sCurrentLine = reader.readLine()) != null) {
                try {
                    //Base.getMachineLoader().getDriver().dispatchCommand(sCurrentLine);
//                    System.out.println(sCurrentLine);
                    machine.runCommand(new replicatorg.drivers.commands.DispatchCommand(sCurrentLine,COM.DEFAULT));
                    n_lines++;
                } catch (Exception e) {
                    break;
                }
            }
            Base.cleanDirectoryTempFiles(Base.getAppDataDirectory() + "/3DModels/");


        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }
        try {
            machine.runCommand(new replicatorg.drivers.commands.SetBusy(false));
            machine.runCommand(new replicatorg.drivers.commands.ReadStatus());

            reader.close();


        } catch (IOException ex) {
            return n_lines;
        }
        return n_lines;

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jProgressBar1 = new javax.swing.JProgressBar();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(571, 466));
        setUndecorated(true);
        setResizable(false);

        jPanel2.setBackground(new java.awt.Color(255, 203, 5));
        jPanel2.setMinimumSize(new java.awt.Dimension(20, 38));
        jPanel2.setPreferredSize(new java.awt.Dimension(567, 38));

        jLabel19.setForeground(new java.awt.Color(0, 0, 0));
        jLabel19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/replicatorg/app/ui/panels/b_simple_18.png"))); // NOI18N
        jLabel19.setText("SAIR");
        jLabel19.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel19.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel19MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel19MouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel19MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel19)
                .addContainerGap(519, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(jLabel19)
                .addGap(20, 20, 20))
        );

        jPanel1.setBackground(new java.awt.Color(248, 248, 248));

        jLabel1.setText("PREPARACAO");
        jLabel1.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/replicatorg/app/ui/panels/bico_extrusao.png"))); // NOI18N

        jProgressBar1.setBackground(new java.awt.Color(186, 186, 186));
        jProgressBar1.setPreferredSize(new java.awt.Dimension(150, 18));
        jProgressBar1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jProgressBar1StateChanged(evt);
            }
        });

        jSeparator2.setBackground(new java.awt.Color(255, 255, 255));
        jSeparator2.setForeground(new java.awt.Color(222, 222, 222));
        jSeparator2.setMinimumSize(new java.awt.Dimension(4, 1));
        jSeparator2.setPreferredSize(new java.awt.Dimension(50, 1));

        jLabel3.setText("Temperatura da Cabeca de Impressao");

        jLabel4.setText("Suspendisse potenti.");
        jLabel4.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        jPanel4.setBackground(new java.awt.Color(255, 203, 5));
        jPanel4.setMinimumSize(new java.awt.Dimension(62, 26));
        jPanel4.setPreferredSize(new java.awt.Dimension(70, 30));
        jPanel4.setRequestFocusEnabled(false);

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/replicatorg/app/ui/panels/b_simple_11.png"))); // NOI18N
        jLabel13.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel13MousePressed(evt);
            }
        });

        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/replicatorg/app/ui/panels/b_simple_10.png"))); // NOI18N

        jLabel15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/replicatorg/app/ui/panels/b_simple_9.png"))); // NOI18N
        jLabel15.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel15MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 7, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, 13, Short.MAX_VALUE)
                            .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(255, 203, 5));
        jPanel3.setPreferredSize(new java.awt.Dimension(169, 17));

        jLabel7.setForeground(new java.awt.Color(0, 0, 0));
        jLabel7.setText("Heating...Please wait.");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jLabel7)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(186, 186, 186)
                        .addComponent(jLabel2))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel3))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(96, 96, 96)
                        .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(43, 43, 43)
                .addComponent(jLabel2)
                .addGap(54, 54, 54)
                .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3)
                .addGap(6, 6, 6)
                .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)
                .addGap(38, 38, 38))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 571, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jProgressBar1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jProgressBar1StateChanged
//        if (jProgressBar1.getValue() == 50) {
//            jLabel1.setText(Languager.getTagValue("Print", "Print_Title"));
//            
//            String sCurrentLine = "";
//            try {
//                machine.runCommand(new replicatorg.drivers.commands.SetBusy(true));
//                while ((sCurrentLine = reader.readLine()) != null) {
//                    try {
//                        Base.getMachineLoader().getDriver().dispatchCommand(sCurrentLine);
//                          //machine.runCommand(new replicatorg.drivers.commands.DispatchCommand(sCurrentLine));                        
//                    } catch (Exception e) {
//                        break;
//                    }
//                }
//                Base.cleanDirectoryTempFiles(Base.getApplicationDirectory()+"/3DModels/");
//                
//
//            } catch (FileNotFoundException ex) {
//            } catch (IOException ex) {
//            }
//            try {
//                reader.close();
//                Base.setPrintEnded(true);
//                machine.runCommand(new replicatorg.drivers.commands.SetBusy(false));
//                okEnabled = true;
//                
//            } catch (IOException ex) {
//                return;
//            }
//        }
//        else if(jProgressBar1.getValue() == 100)
//        {
//            dispose();
//            CalibrationValidation calVal = new CalibrationValidation();
//            calVal.setVisible(okEnabled);
//            updateThread.stop();
//        }
//        else // already heat
//        {
//            String sCurrentLine = "";
//            try {
//                machine.runCommand(new replicatorg.drivers.commands.SetBusy(true));
//                while ((sCurrentLine = reader.readLine()) != null) {
//                    try {
//                        machine.runCommand(new replicatorg.drivers.commands.DispatchCommand(sCurrentLine) );
//                    } catch (Exception e) {
//                        break;
//                    }
//                }
//            } catch (FileNotFoundException ex) {
//            } catch (IOException ex) {
//            }
//            try {
//                reader.close();
//                Base.setPrintEnded(true);
//                machine.runCommand(new replicatorg.drivers.commands.SetBusy(false));
//                okEnabled = true;
//                
//            } catch (IOException ex) {
//                return;
//            }                
//            Base.cleanDirectoryTempFiles(Base.getApplicationDirectory()+"/3DModels/");
//        }
                

    }//GEN-LAST:event_jProgressBar1StateChanged
   
    private void jLabel19MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel19MouseEntered
        jLabel19.setIcon(new ImageIcon(GraphicDesignComponents.getImage("panels", "b_hover_18.png")));
    }//GEN-LAST:event_jLabel19MouseEntered

    private void jLabel19MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel19MouseExited
        jLabel19.setIcon(new ImageIcon(GraphicDesignComponents.getImage("panels", "b_simple_18.png")));
    }//GEN-LAST:event_jLabel19MouseExited

    private void jLabel19MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel19MousePressed
        updateThread.stop();
        dispose();
        machine.killSwitch();
        finalizeHeat();
        Base.getMainWindow().handleStop();
        machine.runCommand(new replicatorg.drivers.commands.SetTemperature(0));  
        Point5d b = machine.getTablePoints("safe");
        double acLow = machine.getAcceleration("acLow");
        double acHigh = machine.getAcceleration("acHigh");
        double spHigh = machine.getFeedrate("spHigh");
        
        // Sleep befpore home after M112
        try {
            Thread.sleep(500);
        } catch (InterruptedException ex) {
            Logger.getLogger(PrintSplashSimpleWaiting.class.getName()).log(Level.SEVERE, null, ex);
        }
        
//        machine.runCommand(new replicatorg.drivers.commands.DispatchCommand("M206 x"+acLow));
//        machine.runCommand(new replicatorg.drivers.commands.SetFeedrate(spHigh));
//        machine.runCommand(new replicatorg.drivers.commands.QueuePoint(b));
//        machine.runCommand(new replicatorg.drivers.commands.DispatchCommand("M206 x"+acHigh));
        machine.runCommand(new replicatorg.drivers.commands.DispatchCommand("G28"));
       
       Base.getMainWindow().getButtons().updatePressedStateButton("quick_guide");
       Base.getMainWindow().getButtons().updatePressedStateButton("maintenance");
       Base.getMainWindow().setEnabled(true);
        if(ProperDefault.get("maintenance").equals("1"))
            ProperDefault.remove("maintenance");    
        Base.bringAllWindowsToFront();
        
        
        
        
        
    }//GEN-LAST:event_jLabel19MousePressed

    private void jLabel13MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel13MousePressed
        setState(ICONIFIED);
    }//GEN-LAST:event_jLabel13MousePressed

    private void jLabel15MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel15MousePressed
        updateThread.stop();
        dispose();
        machine.killSwitch();
        finalizeHeat();
        Base.getMainWindow().handleStop();
        machine.runCommand(new replicatorg.drivers.commands.SetTemperature(0));  
        Point5d b = machine.getTablePoints("safe");
        double acLow = machine.getAcceleration("acLow");
        double acHigh = machine.getAcceleration("acHigh");
        double spHigh = machine.getFeedrate("spHigh");
        
        machine.runCommand(new replicatorg.drivers.commands.DispatchCommand("M206 x"+acLow));
        machine.runCommand(new replicatorg.drivers.commands.SetFeedrate(spHigh));
        machine.runCommand(new replicatorg.drivers.commands.QueuePoint(b));
        machine.runCommand(new replicatorg.drivers.commands.DispatchCommand("M206 x"+acHigh));
        machine.runCommand(new replicatorg.drivers.commands.DispatchCommand("G28"));
        dispose();
        finalizeHeat();
        Base.getMainWindow().handleStop();    
        if(ProperDefault.get("maintenance").equals("1"))
            ProperDefault.remove("maintenance");
        Base.bringAllWindowsToFront();        
    }//GEN-LAST:event_jLabel15MousePressed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JSeparator jSeparator2;
    // End of variables declaration//GEN-END:variables
}

class PrintThread extends Thread {

    CalibrationPrintTest window;
    MachineInterface machine;
    
    public PrintThread(CalibrationPrintTest w,MachineInterface machine) {
        super("Control Panel Update Thread");
        window = w;
        this.machine = machine;
        Base.writeLog("Reading Temperature ...");
    }

    @Override
    public void run() {

        boolean temperatureAchieved = false;
        // we'll break on interrupts
        while (!temperatureAchieved) {
            try {
                window.updateHeatBar();
                temperatureAchieved = window.getAchievement();
                Thread.sleep(200);
            } catch (Exception e) {
                Base.writeLog("Exception occured while reading Temperature ...");
                try {
                    this.join();
                } catch (InterruptedException ex) {
                    Logger.getLogger(UpdateThread.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            window.showMessage();
        }
        Base.writeLog("Temperature achieved...");
        window.sinalizeHeatSuccess();

        
        int n_lines = window.printTest();
        
        while (true) {
            //machine.runCommand(new replicatorg.drivers.commands.ReadStatus());
            try {
                Thread.sleep(250);
            } catch (InterruptedException ex) {
                Logger.getLogger(DisposeFeedbackThread.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (!machine.getDriverQueryInterface().getMachineStatus() || machine.getDriverQueryInterface().isBusy()) {
                window.showPrintMessage();
                try {
                    Thread.sleep(1000);
                    window.updatePrintBar(100 * machine.getStopwatch()/n_lines);
                } catch (InterruptedException ex) {
                    Logger.getLogger(PrintThread.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else {
                window.sinalizeHeatSuccess();
                window.disableMessageDisplay();
                //when the print is over do this
            Base.setPrintEnded(true);
            window.okEnabled = true;
            window.dispose();
//            
            CalibrationValidation calVal = new CalibrationValidation();
            calVal.setVisible(window.okEnabled);
//            
//            updateThread.stop(); 
                break;
                
            }
        }
        
        this.stop();
        this.interrupt();
    }
}

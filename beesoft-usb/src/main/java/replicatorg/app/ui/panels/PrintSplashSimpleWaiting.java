package replicatorg.app.ui.panels;

import java.awt.Color;
import java.awt.Dimension;
import static java.awt.Frame.ICONIFIED;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import pt.bitbox.replicatorg.drivers.usb.UsbPassthroughDriver.COM;
import replicatorg.app.Languager;
import replicatorg.app.ui.GraphicDesignComponents;
import replicatorg.app.Base;
import replicatorg.app.DoNotSleep;
import replicatorg.app.Printer;
import replicatorg.app.PrintEstimator;
import replicatorg.app.ProperDefault;
import replicatorg.machine.MachineInterface;
import replicatorg.util.Point5d;

/**
 * Copyright (c) 2013 BEEVC - Electronic Systems This file is part of BEESOFT
 * software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version. BEESOFT is
 * distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details. You
 * should have received a copy of the GNU General Public License along with
 * BEESOFT. If not, see <http://www.gnu.org/licenses/>.
 */
public class PrintSplashSimpleWaiting extends javax.swing.JFrame implements WindowListener {

    private Printer prt;
    private boolean printEnded;
    private int posX = 0, posY = 0;
    private double startTimeMillis;
    private double startTimeMillis2;
    private UpdateThread3 ut;
    private int progression;
    private static final String FORMAT = "%2d:%2d";
    private int remainingTime = 0;

    public PrintSplashSimpleWaiting(ArrayList<String> preferences, Printer p) {
        initComponents();
        setFont();
        setTextLanguage();
        centerOnScreen();
        prt = p;
        setProgressBarColor();
        printEnded = false;
        enableDrag();
        addWindowListener(this);
        ut = new UpdateThread3(this);
        ut.start();
        setIconImage(new ImageIcon(Base.getImage("images/icon.png", this)).getImage());
    }

    private void setFont() {
        jLabel1.setFont(GraphicDesignComponents.getSSProBold("14"));
        jLabel6.setFont(GraphicDesignComponents.getSSProRegular("11"));
        jLabel7.setFont(GraphicDesignComponents.getSSProRegular("11"));
        jLabel8.setFont(GraphicDesignComponents.getSSProRegular("11"));
        jLabel9.setFont(GraphicDesignComponents.getSSProRegular("11"));
        jLabel11.setFont(GraphicDesignComponents.getSSProRegular("12"));
        jLabel12.setFont(GraphicDesignComponents.getSSProRegular("12"));
    }

    private void setTextLanguage() {
        jLabel1.setText(Languager.getTagValue("Print", "Print_Splash_Info2"));
        jLabel6.setText(Languager.getTagValue("Print", "Print_Estimation"));
        jLabel7.setText(Languager.getTagValue("Print", "Print_Remaining"));
        jLabel11.setText(Languager.getTagValue("OptionPaneButtons", "Line3"));
        jLabel12.setText(Languager.getTagValue("OptionPaneButtons", "Line6"));
    }

    private void centerOnScreen() {
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

        // Determine the new location of the window
        int w = this.getSize().width;
        int h = this.getSize().height;
        int x = (dim.width - w) / 2;
        int y = (dim.height - h) / 2;

        // Move the window
        this.setLocationRelativeTo(null);
        this.setLocationRelativeTo(Base.getMainWindow());
        Base.setMainWindowNOK();
    }

    public void setProgression(int prog) {
        progression = prog;
    }

    private void setProgressBarColor() {
        jProgressBar1.setForeground(new Color(255, 203, 5));
    }

    public void updatePrintBar(double progression) {
        jProgressBar1.setValue((int) (progression));

    }

    private String minutesToHours(int minutes, boolean format) {
        int hoursTrunked = 0;
        int minutesTrunked = minutes;
        double quocient = 0;
        final int BASE = 60;

        if (format) {
            int hours = minutes / 60;
            int minute = minutes % 60;
            return String.format(FORMAT, hours, minute);
        } else {
            if (minutes > BASE) {
                quocient = minutes / 60;
                int hoursCarry = (int) quocient;

                hoursTrunked += hoursCarry;
                minutesTrunked = (int) Math.round(minutes % BASE);

                if (hoursTrunked >= 1) {
                    return String.valueOf(hoursTrunked).concat(":").concat(String.valueOf(minutesTrunked));
                }
            }
            return String.valueOf(minutesTrunked);
        }
    }

    public File getPrintFile() {

        StringBuffer gcode = Base.getMainWindow().getBed().getGcode();
//        System.out.println(gcode);
        File gcodeFile = new File(Base.getAppDataDirectory() + "/3DModels/temp.gcode");

        if (Base.getMainWindow().getBed().isGcodeOK()) {
            PrintWriter out = null;
            try {
                out = new PrintWriter(gcodeFile);

                String[] code = gcode.toString().split(":");

                for (int i = 0; i < code.length; i++) {
                    out.println(code[i]);
                }

            } catch (IOException ex) {
                Logger.getLogger(PrintSplashSimpleWaiting.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                out.close();

            }
            prt.setGCodeFile(gcodeFile);

            // Estimate print duration
            PrintEstimator.estimateTime(gcodeFile);

        } else {
            return prt.getGCode();
        }
        return gcodeFile;
    }

    public void enableSleep() {
        try {

            DoNotSleep ds = new DoNotSleep();
            ds.EnabledSleep();

            Base.writeLog("Sleep started!");

        } catch (Exception ex) {
            Base.writeLog("Error starting Sleep!");
        }
    }

    public void disableSleep() {
        try {

            DoNotSleep ds = new DoNotSleep();
            ds.DisableSleep();

            Base.writeLog("Sleep stoped!");

        } catch (Exception ex) {
            Base.writeLog("Error stoping Sleep!");
        }
    }

    private void enableDrag() {
        this.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                posX = e.getX();
                posY = e.getY();
            }
        });


        this.addMouseMotionListener(new MouseAdapter() {
            public void mouseDragged(MouseEvent evt) {
                //sets frame position when mouse dragged			
                setLocation(evt.getXOnScreen() - posX, evt.getYOnScreen() - posY);
            }
        });
    }

    public int getNGCodeLines() {
        return prt.getGCodeNLines();
    }

    public void startPrintCounter() {
        startTimeMillis = System.currentTimeMillis();
        startTimeMillis2 = System.currentTimeMillis();
    }

    private String printTime() {
        return String.valueOf((int) (Math.ceil((System.currentTimeMillis() - startTimeMillis) / 1000) / 60) + 1);
    }

    public void checkTimeElapsed() {
        if ((int) (Math.ceil((System.currentTimeMillis() - startTimeMillis2) / 1000)) == 60) {
            
            // Not able to estimate print time
            // 00 - error code
            if(remainingTime == 00)
            {
                jLabel7.setText(Languager.getTagValue("Print", "Print_Info"));
                jLabel9.setVisible(false);  
            }
            else
            {
                // Subtract one minute to remaining time
                remainingTime -= 1;
                if (remainingTime > 0) {
                    updatePrintEstimation(String.valueOf(remainingTime), false);
                } else {
                    jLabel7.setText(Languager.getTagValue("Print", "Print_Info2"));
                    jLabel9.setVisible(false);
                }
            }
            startTimeMillis2 = System.currentTimeMillis();
        }
    }

    public void setPrintEnded(boolean status) {
        printEnded = status;

        if (printEnded) {
            jPanel5.setVisible(false);
            int duration = Integer.valueOf(printTime());
            String hours = minutesToHours(duration, true).split("\\:")[0];
            String minutes = minutesToHours(duration, true).split("\\:")[1];

            if (duration >= 2 && duration > 60) {

                jLabel1.setText(Languager.getTagValue("Print", "Print_BuildFinished"));
                jLabel6.setText(Languager.getTagValue("Print", "Print_Completion") + " " + hours + " "
                        + Languager.getTagValue("Print", "PrintHours") + " " + minutes + " " + Languager.getTagValue("Print", "PrintMinutes"));
                jLabel12.setIcon(new ImageIcon(GraphicDesignComponents.getImage("panels", "b_simple_14.png")));
            } else {
                jLabel1.setText(Languager.getTagValue("Print", "Print_BuildFinished"));
                jLabel6.setText(Languager.getTagValue("Print", "Print_Completion") + " " + minutes + " " + Languager.getTagValue("Print", "PrintMinutes"));
                jLabel12.setIcon(new ImageIcon(GraphicDesignComponents.getImage("panels", "b_simple_14.png")));
            }
            ProperDefault.put("durationLastPrint", String.valueOf(duration));
        } else if (!printEnded) {
            abortPrint();
        }
    }

    public void abortPrint() {
        dispose();
        Base.getMainWindow().setEnabled(true);
        Base.getMainWindow().doPreheat(false);
        Base.getMainWindow().handleStop();
//        Base.getMainWindow().doStop();
        Base.getMainWindow().getButtons().updatePressedStateButton("print");
        enableSleep();
    }

    public void updatePrintEstimation(String printEstimation, boolean cutout) {
        String[] duration = null;
        String textE = "";
        String textR = "";

        if (!printEstimation.contains("NA")) {
            if (printEstimation.contains(":")) {
                duration = printEstimation.split(":");
                int hours = Integer.valueOf(duration[0]);
                int minutes = Integer.valueOf(duration[1]);

                remainingTime = hours * 60 + minutes;

                if (hours > 1) {
                    if (cutout) {
                        textE = hours
                                + " " + Languager.getTagValue("Print", "PrintHours")
                                + " " + minutes
                                + " " + Languager.getTagValue("Print", "PrintMinutes");
                        textR = hours
                                + " " + Languager.getTagValue("Print", "PrintHours")
                                + " " + minutes
                                + " " + Languager.getTagValue("Print", "PrintMinutes");

                        jLabel9.setText(textR);
                        jLabel8.setText(textE);
                    } else {
                        textR = hours
                                + " " + Languager.getTagValue("Print", "PrintHours")
                                + " " + minutes
                                + " " + Languager.getTagValue("Print", "PrintMinutes");

                        jLabel9.setText(textR);
                    }
                } else {
                    if (cutout) {
                        textE = hours
                                + " " + Languager.getTagValue("Print", "PrintHour")
                                + " " + minutes
                                + " " + Languager.getTagValue("Print", "PrintMinutes");

                        jLabel8.setText(textE);
                    } else {
                        textR = hours
                                + " " + Languager.getTagValue("Print", "PrintHour")
                                + " " + minutes
                                + " " + Languager.getTagValue("Print", "PrintMinutes");

                        jLabel9.setText(textR);
                    }
                }
            } else {
                remainingTime = Integer.valueOf(printEstimation);
                int hours = 0;
                int minutes = 0;

                if (printEstimation.contains(":")) {
                    duration = printEstimation.split(":");
                    hours = Integer.valueOf(duration[0]);
                    minutes = Integer.valueOf(duration[1]);
                } else {
                    if (remainingTime > 60) {
                        printEstimation = minutesToHours(remainingTime, false);
                        duration = printEstimation.split(":");
                        hours = Integer.valueOf(duration[0]);
                        minutes = Integer.valueOf(duration[1]);
                    }
                }

                if (cutout) {
                    textE = printEstimation
                            + " " + Languager.getTagValue("Print", "PrintMinutes");
                    textR = hours
                            + " " + Languager.getTagValue("Print", "PrintHours")
                            + " " + minutes
                            + " " + Languager.getTagValue("Print", "PrintMinutes");
                    jLabel8.setText(textE);
                    jLabel9.setText(textR);
                } else {
                    if (remainingTime > 60) {
                        textR = hours
                                + " " + Languager.getTagValue("Print", "PrintHours")
                                + " " + minutes
                                + " " + Languager.getTagValue("Print", "PrintMinutes");
                    }
                    if (remainingTime > 1 && remainingTime <= 60) {
                        textR = remainingTime
                                + " " + Languager.getTagValue("Print", "PrintMinutes");
                    } else if (remainingTime == 1) {
                        textR = remainingTime
                                + " " + Languager.getTagValue("Print", "PrintMinute");
                    }
                    // no need for else. Parse is being made before calling thin method

                    jLabel9.setText(textR);
//                else
//                {
//                     textR = Languager.getTagValue("Print", "Print_Info");
//                }

                }
            }
        }
        else
        {
            remainingTime = 00;
        }

    }

    @Override
    public void windowOpened(WindowEvent e) {
        return;
    }

    @Override
    public void windowClosing(WindowEvent e) {
        return;
    }

    @Override
    public void windowClosed(WindowEvent e) {
        return;
    }

    @Override
    public void windowIconified(WindowEvent e) {
        Base.getMainWindow().setState(JFrame.ICONIFIED);
        return;
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        jProgressBar1.setValue(progression);
        Base.getMainWindow().setExtendedState(JFrame.MAXIMIZED_BOTH);

    }

    @Override
    public void windowActivated(WindowEvent e) {
        return;
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        return;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator1 = new javax.swing.JSeparator();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jProgressBar1 = new javax.swing.JProgressBar();
        jPanel4 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        setPreferredSize(new java.awt.Dimension(500, 203));
        setResizable(false);

        jPanel2.setBackground(new java.awt.Color(248, 248, 248));
        jPanel2.setPreferredSize(new java.awt.Dimension(475, 169));

        jLabel1.setText("A PROCESSAR... POR FAVOR AGUARDE");

        jProgressBar1.setBackground(new java.awt.Color(186, 186, 186));

        jPanel4.setBackground(new java.awt.Color(255, 203, 5));
        jPanel4.setMinimumSize(new java.awt.Dimension(62, 26));
        jPanel4.setRequestFocusEnabled(false);

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/replicatorg/app/ui/panels/b_simple_11.png"))); // NOI18N
        jLabel13.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel13MousePressed(evt);
            }
        });

        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/replicatorg/app/ui/panels/b_simple_10.png"))); // NOI18N

        jLabel15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/replicatorg/app/ui/panels/b_simple_9.png"))); // NOI18N
        jLabel15.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel15MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 7, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, 13, Short.MAX_VALUE)
                            .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jPanel1.setBackground(new java.awt.Color(248, 248, 248));
        jPanel1.setPreferredSize(new java.awt.Dimension(117, 22));

        jLabel6.setText("jLabel6");

        jLabel8.setText("NA");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel8)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel8))
                .addContainerGap())
        );

        jPanel5.setBackground(new java.awt.Color(248, 248, 248));

        jLabel7.setText("jLabel7");

        jLabel9.setText("NA");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel9)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel9))
                .addGap(5, 5, 5))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 290, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 290, Short.MAX_VALUE))
                .addGap(90, 90, 90))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 471, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(15, 15, 15)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 8, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22))
        );

        jPanel3.setBackground(new java.awt.Color(255, 203, 5));
        jPanel3.setMinimumSize(new java.awt.Dimension(20, 46));
        jPanel3.setPreferredSize(new java.awt.Dimension(423, 127));

        jLabel11.setForeground(new java.awt.Color(0, 0, 0));
        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/replicatorg/app/ui/panels/b_simple_18.png"))); // NOI18N
        jLabel11.setText("CANCELAR");
        jLabel11.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel11MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel11MouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel11MousePressed(evt);
            }
        });

        jLabel12.setForeground(new java.awt.Color(0, 0, 0));
        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/replicatorg/app/ui/panels/b_disabled_14.png"))); // NOI18N
        jLabel12.setText("OK");
        jLabel12.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel12.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel12MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel12MouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel12MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel11)
                .addGap(12, 12, 12)
                .addComponent(jLabel12)
                .addGap(12, 12, 12))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel11))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 495, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 495, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel12MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel12MouseEntered
        if (printEnded) {
            jLabel12.setIcon(new ImageIcon(GraphicDesignComponents.getImage("panels", "b_hover_14.png")));
        }
    }//GEN-LAST:event_jLabel12MouseEntered

    private void jLabel12MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel12MouseExited
        if (printEnded) {
            jLabel12.setIcon(new ImageIcon(GraphicDesignComponents.getImage("panels", "b_simple_14.png")));
        }
    }//GEN-LAST:event_jLabel12MouseExited

    private void jLabel11MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel11MouseEntered
        jLabel11.setIcon(new ImageIcon(GraphicDesignComponents.getImage("panels", "b_hover_18.png")));
    }//GEN-LAST:event_jLabel11MouseEntered

    private void jLabel11MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel11MouseExited
        jLabel11.setIcon(new ImageIcon(GraphicDesignComponents.getImage("panels", "b_simple_18.png")));
    }//GEN-LAST:event_jLabel11MouseExited

    private void jLabel11MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel11MousePressed
        dispose();
        MachineInterface machine = Base.getMainWindow().getMachine();
        Base.bringAllWindowsToFront();
        Base.getMainWindow().handleStop();
        Base.getMainWindow().setEnabled(true);
//        Base.getMainWindow().doPreheat(false); //handleStop is doing it
        Base.getMainWindow().getButtons().updatePressedStateButton("print");
        ut.stop();

        Base.cleanDirectoryTempFiles(Base.getAppDataDirectory() + "/3DModels/");

        enableSleep();
        Point5d b = machine.getTablePoints("safe");
        double acLow = machine.getAcceleration("acLow");
        double acHigh = machine.getAcceleration("acHigh");
        double spHigh = machine.getFeedrate("spHigh");

        // Sleep befpore home after M112
        try {
            Thread.sleep(500);
        } catch (InterruptedException ex) {
            Logger.getLogger(PrintSplashSimpleWaiting.class.getName()).log(Level.SEVERE, null, ex);
        }

//        machine.runCommand(new replicatorg.drivers.commands.DispatchCommand("M206 x" + acLow));
//        machine.runCommand(new replicatorg.drivers.commands.SetFeedrate(spHigh));
//        machine.runCommand(new replicatorg.drivers.commands.QueuePoint(b));
//        machine.runCommand(new replicatorg.drivers.commands.DispatchCommand("M206 x" + acHigh));
        machine.runCommand(new replicatorg.drivers.commands.DispatchCommand("G28"));
    }//GEN-LAST:event_jLabel11MousePressed

    private void jLabel12MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel12MousePressed
        if (printEnded) {
            dispose();
            Base.getMainWindow().setEnabled(true);
            enableSleep();
            Base.getMachineLoader().getMachineInterface().runCommand(new replicatorg.drivers.commands.SetTemperature(0));
            Base.getMainWindow().getButtons().updatePressedStateButton("print");
            Base.cleanDirectoryTempFiles(Base.getAppDataDirectory() + "/3DModels/");
            Base.bringAllWindowsToFront();
        }
    }//GEN-LAST:event_jLabel12MousePressed

    private void jLabel15MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel15MousePressed
        dispose();
        MachineInterface machine = Base.getMainWindow().getMachine();
        Base.getMainWindow().handleStop();
        Base.getMainWindow().setEnabled(true);
        //        Base.getMainWindow().doPreheat(false);  //handleStop is doing it
        Base.bringAllWindowsToFront();
        //        Base.getMainWindow().doStop();
        ut.stop();
        Base.getMainWindow().getButtons().updatePressedStateButton("print");
        enableSleep();
        Point5d b = machine.getTablePoints("safe");
        double acLow = machine.getAcceleration("acLow");
        double acHigh = machine.getAcceleration("acHigh");
        double spHigh = machine.getFeedrate("spHigh");

        machine.runCommand(new replicatorg.drivers.commands.DispatchCommand("M206 x" + acLow));
        machine.runCommand(new replicatorg.drivers.commands.SetFeedrate(spHigh));
        machine.runCommand(new replicatorg.drivers.commands.QueuePoint(b));
        machine.runCommand(new replicatorg.drivers.commands.DispatchCommand("M206 x" + acHigh));
        machine.runCommand(new replicatorg.drivers.commands.DispatchCommand("G28"));
    }//GEN-LAST:event_jLabel15MousePressed

    private void jLabel13MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel13MousePressed
        setState(ICONIFIED);
        Base.getMainWindow().deactivateCameraControls();
    }//GEN-LAST:event_jLabel13MousePressed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JSeparator jSeparator1;
    // End of variables declaration//GEN-END:variables
}

class UpdateThread3 extends Thread {

    PrintSplashSimpleWaiting window;

    public UpdateThread3(PrintSplashSimpleWaiting w) {
        super("Update Thread");
        window = w;
    }

    @Override
    public void run() {
        try {
            window.startPrintCounter();
            int nLines = 0;
            boolean finished = false;
            BufferedReader reader = null;
            String sCurrentLine = "";

            try {
                reader = new BufferedReader(new FileReader(window.getPrintFile()));
                window.updatePrintEstimation(PrintEstimator.getEstimatedTime(), true);
            } catch (FileNotFoundException ex) {
                Base.writeLog("Can't read gCode file to print");
            }

            nLines = window.getNGCodeLines();
            Base.getMachineLoader().getMachineInterface().setStopwatch(0);

            while ((sCurrentLine = reader.readLine()) != null) {
                Base.getMachineLoader().getMachineInterface().runCommand(new replicatorg.drivers.commands.DispatchCommand(sCurrentLine, COM.NO_RESPONSE));
            }

            double lines = 0.0;

            while (true) {
                Thread.sleep(1000);

                lines = (double) Base.getMachineLoader().getMachineInterface().getStopwatch();

                window.updatePrintBar((lines / (double) nLines) * 100);
                window.setProgression((int) ((lines / (double) nLines) * 100));

                window.checkTimeElapsed();

                if (Base.getMachineLoader().getMachineInterface().getStopwatch() >= nLines) {
                    finished = true;
                    break;
                }
            }

            reader.close();
            Base.setPrintEnded(true);
            window.setPrintEnded(finished);
            this.join();

        } catch (IOException ex) {
            Logger.getLogger(UpdateThread3.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(UpdateThread3.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}

package replicatorg.app.ui;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import net.miginfocom.swing.MigLayout;
import replicatorg.app.Base;
import replicatorg.app.ProperDefault;
import replicatorg.app.ui.mainWindow.UpdateChecker;
import replicatorg.app.ui.panels.TourWelcome;
import replicatorg.app.ui.panels.WelcomeQuickguide;

/**
* Copyright (c) 2013 BEEVC - Electronic Systems
* This file is part of BEESOFT software: you can redistribute it and/or modify 
* it under the terms of the GNU General Public License as published by the 
* Free Software Foundation, either version 3 of the License, or (at your option)
* any later version. BEESOFT is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
* for more details. You should have received a copy of the GNU General
* Public License along with BEESOFT. If not, see <http://www.gnu.org/licenses/>.
*/
public class WelcomeSplash extends JFrame {

    private MainWindow window;
    private WelcomeQuickguide guideWizard;
    private ImageIcon image;
    private JLabel label;
    private JProgressBar bar;
    private int newWidth = 600;
    private int newHeight = 333;
    private int duration = 80;
    
    /**
     * Welcome Splash init
     * @param wind MainWindow for visual and feature control
     */
    public WelcomeSplash(MainWindow wind) {
        
        Base.writeLog("Welcome Splash started ...");
        setUndecorated(true);
        window = wind;
        label = new JLabel();
        bar = new JProgressBar();
        // Loads Splash image
        image = new ImageIcon(Base.getImage("images/welcomeSplash.png",this));
        Image img = image.getImage();  
        // Loads Splash image for dimensios getter
        BufferedImage img2 = Base.getImage("images/welcomeSplash.png",this);
        // Gets Screen Dimension
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        
        // Calculates ratio for modular adjustement to every screen
        double imageRatio = img2.getWidth() /img2.getHeight();
        double screenRatio = 0.75;
        newWidth = (int)(d.width * screenRatio);
        double scale = d.width * screenRatio / img2.getWidth();
        newHeight = (int)(img2.getHeight() * scale  );
  
        // Scales original image to new size values with Smooth conversion
        Image newimg = img.getScaledInstance(newWidth, newHeight, java.awt.Image.SCALE_SMOOTH);  
        image = new ImageIcon(newimg);  
       
        // Sets bar preferences and size.
        // Bar width is equal to image width. Height has value = 5
        bar.setMaximum(newWidth-5);
        bar.setPreferredSize(new Dimension(newWidth-5,5));
        label.setIcon(image);
        
        // Sets JWindow Layout and add components
        this.getContentPane().setLayout(new MigLayout("flowy,fillx,center"));
        this.getContentPane().add(label);
        this.getContentPane().add(bar);
        this.setVisible(true);
        this.pack();
        this.setLocationRelativeTo(null);
        
        // Thread to update JProgress Bar
        new Thread()
        {
            @Override
            public void run()
            {
                int i = 0;
                // Increment value for each bar update
                int inc = 0; 
                
                // Number of seconds to hold splash screen
                while(i < getDuration())
                {
                    bar.setValue(inc);
                    i++;
                    inc += getWidth()/80;
                    try {
                        sleep(100);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(WelcomeSplash.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
                changeState();
            }
        }.start();
    }
    
    /**
     * Monitor changes in MainWindow and performs several operations at startup
     */
    public void changeState()
    {
        window.setVisible(true);
        window.setExtendedState(JFrame.MAXIMIZED_BOTH);
//        window.setSize(Toolkit.getDefaultToolkit().getScreenSize().width, Toolkit.getDefaultToolkit().getScreenSize().height);
//        window.setPreferredSize(new Dimension(Toolkit.getDefaultToolkit().getScreenSize().width, Toolkit.getDefaultToolkit().getScreenSize().height));
//        window.setLocation(0, 0);
        this.setVisible(false);
        Base.writeLog("BEESOFT main window loaded ... ");
       
        //GuideWizard
        if(Boolean.valueOf(ProperDefault.get("firstTime")))
        {
            Base.writeLog("BEESOFT tour loaded ... ");
//            guideWizard = new WelcomeQuickguide();
//            guideWizard.setVisible(true);
            TourWelcome p = new TourWelcome();
            p.setVisible(true);
        }
        
        /**
         * Check is 3DModels folder exists.
         * If not, create it.
         */
        Base.getAppDataDirectory();
        Base.copy3DFiles();
        
        window.setEnabled(true);
        
        // Checks for software and firmware updates
        if(!Boolean.valueOf(ProperDefault.get("firstTime")))
        {       
            UpdateChecker advise = new UpdateChecker();
            if(advise.updatesAvailable())
            {
                advise.setMessage("Available");
                advise.setVisible(true);
            }
            else
                advise.dispose();
        }
        Base.updateVersions();
    }
    
    /**
     * Get Splash Width
     * @return splash width
     */
    public int getWidth()
    {
        return newWidth;
    }
    
    /**
     * Set Splash Screen duration
     * @param milis Duration
     */
    public void setDuration(int milis)
    {
        this.duration = milis;
    }
    
    /**
     * Get Splash Screen Duration
     * @return duration 
     */
    public int getDuration()
    {
        return duration;
    }
    
}

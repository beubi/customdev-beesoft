
package replicatorg.app;

import javax.swing.JOptionPane;
import com.apple.eawt.ApplicationAdapter;
import com.apple.eawt.ApplicationEvent;

public class MacOSHandler extends ApplicationAdapter
{
  private Base handler;
  
  public MacOSHandler(Base handler)
  {
    this.handler = handler;
  }

  public void handleQuit(ApplicationEvent e)
  {
    System.exit(0);
  }

  public void handleAbout(ApplicationEvent e)
  {
    // tell the system we're handling this, so it won't display
    // the default system "about" dialog after ours is shown.
//    e.setHandled(true);
//    JOptionPane.showMessageDialog(null, "Show About dialog here");
  }

  public void handlePreferences(ApplicationEvent e)
  {
//    JOptionPane.showMessageDialog(null, "Show Preferences dialog here");
  }
}

package replicatorg.app;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import replicatorg.app.ui.MainWindow;
import replicatorg.model.PrintBed;
import replicatorg.plugin.toolpath.ToolpathGenerator;
import replicatorg.plugin.toolpath.ToolpathGeneratorFactory;
import replicatorg.plugin.toolpath.skeinforge.SkeinforgeGenerator.SkeinforgeOption;

/**
* Copyright (c) 2013 BEEVC - Electronic Systems
* This file is part of BEESOFT software: you can redistribute it and/or modify 
* it under the terms of the GNU General Public License as published by the 
* Free Software Foundation, either version 3 of the License, or (at your option)
* any later version. BEESOFT is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
* for more details. You should have received a copy of the GNU General
* Public License along with BEESOFT. If not, see <http://www.gnu.org/licenses/>.
*/
public class Printer {

    private MainWindow mainWindow;
    private PrintBed bed;
    private File gcode;
    private ArrayList<String> params;
    private File stl;
    private ToolpathGenerator generator;

    public Printer(ArrayList<String> printParams) {
        this.mainWindow = Base.getMainWindow();
        this.bed = mainWindow.getBed();
        this.gcode = null;
        this.params = printParams;
    }

    public double generateGCode(ArrayList<String> params) {

        double gcodeEstimation = 0.0;

        stl = generateSTL();
        // params.get(0) - Profile name
        // params.get(1) - Color ratio
        // params.get(2) - infillRatio
        // params.get(3) - Raft:T/F
        // params.get(4) - Support:T/F

        String profile = params.get(0);
        String colorRatio = params.get(1);
        String infillRatio = getInfill(params.get(2));
        String raft = params.get(3);
        String support = params.get(4);
//        List<SkeinforgeOption> base_options = new LinkedList<SkeinforgeOption>();
        List<SkeinforgeOption> top_options = new LinkedList<SkeinforgeOption>(); 
          
        //set color
        top_options.add(new SkeinforgeOption("dimension.csv", "Filament Packing Density (ratio):", colorRatio)); 
        //set infill
        top_options.add(new SkeinforgeOption("fill.csv", "Infill Solidity (ratio):", infillRatio));       
   
        if (profile.equalsIgnoreCase("high")) {
            top_options.add(new SkeinforgeOption("speed.csv", "Perimeter Feed Rate Multiplier (ratio):", "0.25"));
            top_options.add(new SkeinforgeOption("speed.csv", "Perimeter Flow Rate Multiplier (ratio):", "0.25"));            
        }
        
        if (raft.equalsIgnoreCase("true")) {
            top_options.add(new SkeinforgeOption("raft.csv", "Base Layers (integer):", "1"));
            top_options.add(new SkeinforgeOption("raft.csv", "Interface Layers (integer):", "2"));           
        }
       
        if (support.equalsIgnoreCase("true")) {
           
            top_options.add(new SkeinforgeOption("raft.csv", "None", "false"));
            top_options.add(new SkeinforgeOption("raft.csv", "Empty Layers Only", "false"));
            top_options.add(new SkeinforgeOption("raft.csv", "Everywhere", "false"));
            top_options.add(new SkeinforgeOption("raft.csv", "Exterior Only", "true"));
        }
       //debug
//        System.out.println("Options selected:");    
//        for(int i = 0; i<options.size(); i++)
//        {            
//            System.out.println("p:"+options.get(i).getParameter());
//            System.out.println("p:"+options.get(i).getArgument());
//        }
//        
        // Predicts GCode Time
//        gcodeEstimation = Oracle.getGCodeGerationTime(stl);
//        Base.writeLog("Estimated time for GCode generation ----->" + gcodeEstimation);
//        System.out.println("Tempo estimado GCode ----->" + gcodeEstimation);

         gcode = runToolpathGenerator(mainWindow, profile, top_options);
         
         // Estimate print duration
         PrintEstimator.estimateTime(gcode);
         
         if(gcode != null)
         {
            Base.writeLog("GCode generated");
//            parseGCodeToSave();
         }
         else
         {
             // handle with no permission error cancelling print and setting error message
             return -1;
         }
        return gcodeEstimation;
    }
    
    public void endGCodeGeneration()
    {
        if(generator != null)
            generator.destroyProcess();
    }
    
    public void setGCodeFile(File gFile)
    {
        this.gcode = gFile;
    }
    
    private File generateSTL() {
        
        File stl = new File(Base.getAppDataDirectory()+"/3DModels/"+ bed.getPrintBedFile().getName().split(".bee")[0]
                + System.currentTimeMillis()+ ".stl");
        FileWriter fw = null;
        BufferedWriter bw = null;
        String code = null;

        // Create physical GCODE File
        try {
            fw = new FileWriter(stl);
            bw = new BufferedWriter(fw);
            bw.write("");
        } catch (FileNotFoundException ex) {
            Base.writeLog("Can't find path "+stl.getName());
        } catch (IOException ex) {
            Base.writeLog("Can create file "+stl.getName());
        }

        try
        {
            // Insert header
            bw.write("solid Default");

            // Parse the rest of it
            for (int i = 0; i < bed.getNumberModels(); i++) {
                byte[] stream = bed.getModel(i).getStream();

                code = new String(stream);

                // Parse header and footer -> remove header and footer
                String codeWHeader = code.split("solid Default")[1];
                String codePlain = codeWHeader.substring(0, codeWHeader.length()-3);

                // write to file
                bw.write(codePlain);
            }
            // Insert footer
            bw.write("endsolid Default");
        } catch (IOException ex) {
            Base.writeLog("Can generate STL to file ");
        }
        try {
            // Close writer
            bw.close();
            fw.close();
        } catch (IOException ex) {
            Base.writeLog("Can close STL writer ");
        }

        return stl;
    }

    
    private File runToolpathGenerator(MainWindow mw, String profile,List params) {

        generator = ToolpathGeneratorFactory.createSelectedGenerator();
        return generator.generateToolpath(stl.getAbsolutePath(), profile, params);
    }
    
    public File getGCode()
    {         
        parseGCodeToSave();
        return gcode;
                
    }

    private void transferGCode(File gcode)
    {
        Base.getMachineLoader().getDriver().executeGCodeLine("");
        Base.getMachineLoader().getDriver().executeGCodeLine("");
        Base.getMachineLoader().getDriver().executeGCodeLine("");
    }

    private String getInfill(String infill) {
   
        /*
         * Infills
         * LOW - 5%
         * MED - 25%
         * HIGH- 45%
         */
        if(infill.contains("LOW"))
            return "0.05";
        else if(infill.contains("MED"))
            return "0.25";
        else if(infill.contains("HIGH"))
            return "0.45";
        
        return "0.05";
    }

    public int getGCodeNLines()
{
        LineNumberReader lnrGCode = null;
        try {
            lnrGCode = new LineNumberReader(new FileReader(gcode));
            lnrGCode.skip(Long.MAX_VALUE);
        } catch (FileNotFoundException ex) {
            Base.writeLog("Cant calculate GCode Number of lines");
        } catch (IOException ex) {
            Base.writeLog("Cant calculate GCode Number of lines");
        }
        return lnrGCode.getLineNumber();
        
    }
     
    private void parseGCodeToSave()  
    {
        StringBuffer code = new StringBuffer();
        
        try {
//            gcode = new File(gcodeBase.getParent()+"/temp.gcode");

            BufferedReader br = new BufferedReader(new FileReader(gcode));
            String line = "";
            while ((line = br.readLine()) != null) {
                    code.append(line);
                    code.append(":");
            }
            Base.getMainWindow().getBed().setGcode(code);
            br.close();
        }
        catch(Exception e)
        {
            Base.writeLog("Error saving GCode to scene "+bed.getPrintBedFile()+ " . Exception: "+e.getMessage());
        }

        saveGCode();
    }

    private void saveGCode() {
        ObjectOutputStream oos;

        try { 
            oos = new ObjectOutputStream(new FileOutputStream(bed.getPrintBedFile()));
            oos.writeObject(bed);
            oos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
